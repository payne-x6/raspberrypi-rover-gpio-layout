# README #

This library for Raspberry Pi 2 is extension of WiringPi library and it is part of my thesis on the topic Rover based on Raspberry Pi 2.
  
### What is this repository for? ###

* GPIOLayout
* 0.1
* For more information open [wiki](https://bitbucket.org/payne-x6/raspberrypi-rover-gpio-layout/wiki/Getting%20started)

### How do I get set up? ###

* At this point it's just bunch of files that copy into your project.
* When compile your project, include WiringPi library. You need to install WiringPi manually before.
* This library is right now available for Raspberry Pi 2 only.


### Who do I talk to? ###

* Repo owner or admin
