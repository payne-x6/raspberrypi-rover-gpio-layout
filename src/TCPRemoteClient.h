/*
 * TCPRemoteClient.h
 *
 *  Created on: Nov 25, 2015
 *  Author: Jan Hak
 */

#ifndef SRC_TCPREMOTECLIENT_H_
#define SRC_TCPREMOTECLIENT_H_

#include "ConnectionErrorException.h"
#include "MessageUnresolvedException.h"
#include "GPIOManager.h"
#include "RPIConstants.h"
#include "MotorController.h"
#include "RPIManager.h"

#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <thread>
#include <memory>
#include <map>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>

// Definition of command types
#define END_BYTE 0

#define GET_NEW_GPIO 1
#define GET_GPIO_TYPE 2
#define GET_GPIO_VALUE 3
#define SET_GPIO_TYPE 4
#define SET_GPIO_VALUE 5

#define MODULE_MOTOR_CONTROLLER_NEW 64
#define MODULE_MOTOR_CONTROLLER_DIRECTION_VECTOR 65

#define TOOL_GET_SETTINGS 128
#define TOOL_SHUTDOWN 129
#define TOOL_PING 130
#define TOOL_PONG 131

using namespace std;

class TCPRemoteClient {
private:
	// Flag if server listening is running
	bool running;
	// Socket of network connection
	int client_socket;
	// Struct that stores address of client
	struct sockaddr_in my_addr;
	// Thread for recieving of messages
	unique_ptr<thread> reciever;
	// Thread that send notifications on server
	unique_ptr<thread> notifier;
	// Flag that show if is client connected on server
	bool connected;

	// Map that stores available GPIO pins for setup remotely from server
	map<int, GPIOPin *> gpioPins;
	// Map that stores abailable MotorControllers accessible remotely from server
	map<int, unique_ptr<MotorController>> motorControllers;

	/**
	 * Main method of reciever thread
	 **/
	void run();
	/**
	 * Main method of notifier thread
	 **/
	void clientNotification();

	/**
	 * Method for parsing SET_GPIO_TYPE message recieved from server
	 **/
	void recvSetGPIOType();
	/**
	 * Method for parsing SET_GPIO_VALUE message recieved from server
	 **/
	void recvSetGPIOValue();
	/**
	 * Method for parsing MODULE_MOTOR_CONTROLLER_DIRECTION_VECTOR message recieved from server
	 **/
	void recvMotorDirectionVector();
	/**
	 * Method for parsing TOOL_GET_SETTINGS message recieved from server
	 **/
	void recvGetSettings();
	/**
	 * Method for parsing TOOL_SHUTDOWN message recieved from server
	 **/
	void recvShutdown();
	/**
	 * Method for parsing TOOL_PING message recieved from server
	 **/
	void recvPing();
	/**
	 * Method for parsing TOOL_PONG message recieved from server
	 **/
	void recvPong();
public:
	/**
	 * Create instance of TCP client that is able starts comunication with server on given IP and port
	 *
	 * @param IP or domain name of server
	 * @param port of server
	 * @throw ConnectionErrorException when unable to connect to server
	 **/
	TCPRemoteClient(char *, uint16_t);
	~TCPRemoteClient();

	/**
	 * Starts comunication with server
	 **/
	void start();
	/**
	 * Block actual thread till connection between client and server ends
	 **/
	void wait();
	/**
	 * Stops communication with server
	 **/
	void stop();

	/**
	 * Add GPIO pin that is available setup remotely from server
	 * @param ID of GPIO pin
	 **/
	void addGPIO(unsigned int);
	/**
	 * Add motor controllet that is available setup remotely from server
	 *
	 * @param ID of module
	 * @param ID of GPIO pin for movement of left motor forward
	 * @param ID of GPIO pin for movement of left motor backward
	 * @param ID of GPIO pin for movement of right motor forward
	 * @param ID of GPIO pin for movement of right motor backward
	 **/
	void addMotorController(unsigned int, uint8_t, uint8_t, uint8_t, uint8_t);

	/**
	 * Send information about new GPIO pin to the server
	 * @param New GPIO pin ID
	 **/
	void sendNewGPIO(uint8_t);
	/**
	 * Send information about new Motor controller to the server
	 * @param New Motor Controller ID
	 **/
	void sendNewMotorController(uint8_t);
	/**
	 * Send ping to the server
	 **/
	void sendPing();
	/**
	 * Send pong when recieve ping from server
	 **/
	void sendPong();
};

#endif /* SRC_TCPREMOTECLIENT_H_ */
