/*
 * LoadConfigurationException.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *
 *	Exception thrown when loading of configuration from file get parse or store data error 
 */

#ifndef LOAD_CONFIGURATION_EXCEPTION_H_
#define LOAD_CONFIGURATION_EXCEPTION_H_

#include <exception>
#include <string>

using namespace std;

class LoadConfigurationException : public exception {
private:
    // Text of exception
	string text;
public:
	LoadConfigurationException()
		: text("Unable to load configuration file") {};
	LoadConfigurationException(const char *ex) : text(ex){};
	virtual const char* what() const throw() {
		return text.c_str();
	}
};

#endif /** LOAD_CONFIGURATION_EXCEPTION_H_ **/
