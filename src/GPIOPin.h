/*
 * GPIOPin.h
 *
 *  Created on: Oct 13, 2015
 *  Author: Jan Hak
 *  Class that makes operation over GPIO pins and lock its usage among all processes
 */

#ifndef GPIOPIN_H_
#define GPIOPIN_H_

#include "RPIConstants.h"
#include "IllegalGPIOStateException.h"

#include <wiringPi.h>
#include <softPwm.h>

#include <semaphore.h>

using namespace std;

class GPIOPin {
private:
	// Counter of pins
	static int COUNT;
	// ID of pin
	int pin;
	// Lock for operations over this pin among all processes
	sem_t lock;
	// Setup of GPIO pin (INPUT, OUTPUT, PWM)
	RPIRover::GPIOType type;
	// Value set on pin
	int value;
public:
	GPIOPin();
	virtual ~GPIOPin();

	/**
	 * This method return unique ID of pin
	 * @return ID of pin
	 **/
	int getPin();
	/**
	 * Return setup type of pin
	 * @return enumeration GPIOType indicating setup of pin
	 **/
	RPIRover::GPIOType getType();
	/**
	 * Set type of pin with usage of lock
	 * @param enumeration GPIOType indication setup of pin
	 **/
	void setType(RPIRover::GPIOType);
	/**
	 * Return value setup on pin. O when LOW voltage, 1 when HIGH voltage, <0, 100> for PWM
	 * @return value setup on pin
	 * @throw IllegalGPIOStateException when pin is PWM - can't be remowed from PWM list
	 **/
	int getValue();
	/**
	 * Set value on pin. For OUTPUT 0 is LOW other is HIGH, for PWM <0, 100>
	 * @param value to be setup on pin.
	 * @throw IllegalGPIOStateException when pin is INPUT
	 **/
	void setValue(int);
	/** 
	 * Read value from INPUT pin
	 * @return value on INPUT pin
	 * @throw IllegalGPIOStateException when pin is not INPUT
	 **/
	int read();
};

#endif /* GPIOPIN_H_ */
