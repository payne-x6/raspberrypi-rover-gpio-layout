/*
 * WrongArgumentsException.h
 *
 *  Created on: Nov 25, 2015
 *  Author: Jan Hak
 * Exception thworn when constructor of class gets wrong arguments that are unable to initialize alocated memory
 */
#ifndef WRONG_ARGUMENTS_EXCEPTION_H_
#define WRONG_ARGUMENTS_EXCEPTION_H_

#include <exception>

using namespace std;

class WrongArgumentsException : public exception {
public:
	WrongArgumentsException() {};
	virtual const char* what() const throw() {
		return "Arguments sent to constructor are wrong!";
	}
};

#endif /** LOAD_CONFIGURATION_EXCEPTION_H_ **/
