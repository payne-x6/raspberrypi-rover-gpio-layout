/*
 * IllegalGPIOStateException.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *
 *	Exception thrown when trying to access GPIO pin in illegal state for that setup
 */

#ifndef ILLEGAL_GPIO_STATE_EXCEPTION_H_
#define ILLEGAL_GPIO_STATE_EXCEPTION_H_

#include <exception>
#include <string>

using namespace std;

class IllegalGPIOStateException : public std::exception {
private:
	// Text of exception
	string text;
public:
	IllegalGPIOStateException()
		: text("Trying to use GPIO pin undefined way") {};
	IllegalGPIOStateException(const char *ex) : text(ex){};
	virtual const char* what() const throw() {
		return text.c_str();
	}
};

#endif /** ILLEGAL_GPIO_STATE_EXCEPTION_H_ **/
