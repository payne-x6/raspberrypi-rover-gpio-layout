/*
 * ConfigLoader.cpp
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 */

#include "ConfigLoader.h"

const string ConfigLoader::delimiter = " ";

ConfigLoader::ConfigLoader() {
	// initialize variables
	ipSet = false;
	portSet = false;
	for(int i = 0; i < 26; i++) {
		gpioMap[i] = false;
	}

	// open file for reading
	ifstream infile("/etc/raspberrycpp/network.conf");
	if(!infile.is_open()) {
		throw LoadConfigurationException("Unable to load file '/etc/raspberrycpp/network.conf'");
	}

	// for each line of configuration file
	int ln = 1;
	string line;
	while(getline(infile, line)) {
		parseLine(ln, line);
		ln++;
	}

	// IP and Port have to be setup
	if(!ipSet || !portSet) {
		throw LoadConfigurationException("Network settings missing");
	}
}

void ConfigLoader::parseLine(const unsigned int ln, const string &line) {
	// Trim line
	string trimed = trim(line);
	int delimiterPosition = trimed.find(delimiter);

	// Get command
	string name = trimed.substr(0, delimiterPosition);
	//Line not empty of comment
	if(name.size() && *(name.begin()) != '#') {
		// Parse rest of line
		if (name == "Bind" && ipAddrBind(trimed.substr(delimiterPosition + 1, trimed.size()))) {
			cout << "IP Adress loaded" << endl;
		}
		else if (name == "Port" && ipPortBind(trimed.substr(delimiterPosition + 1, trimed.size()))) {
			cout << "Network port loaded" << endl;
		}
		else if (name == "MotorController" && addMotorController(trimed.substr(delimiterPosition + 1, trimed.size()))) {
			cout << "MotorController loaded" << endl;
		}
		else {
			cerr << "WARNING: Error load line " << ln << " in configuration file!"
				<< endl;
		}
	}
}

bool ConfigLoader::ipAddrBind(const string &args) {
	// IP set only once
	if(ipSet) {
		return false;
	}
	
	// Resolve domain name and set IP
	struct hostent *addr = gethostbyname(args.c_str());
	if(!addr) {
		return false;
	} else {
		strcpy(this->addr, inet_ntoa(*((struct in_addr *) addr->h_addr_list[0])));
		ipSet = true;
		return true;
	}
}

bool ConfigLoader::ipPortBind(const string &args) {
	// Port set only once
	if(portSet) {
		return false;
	}

	// Parse port from rest of line
	if(sscanf(args.c_str(), "%" SCNu16, &port) != 1) {
		return false;
	}
	if(port > 0xFFFF) {
		return false;
	}

	portSet = true;
	return true;

}

bool ConfigLoader::addMotorController(const string &args) {
	int pinLF,
		pinLB,
		pinRF,
		pinRB;
	// Parse ids of pins
	int returned = sscanf(args.c_str(), "%d %d %d %d", &pinLF, &pinLB, &pinRF, &pinRB);
	if(returned != 4) {
        return false;
	}

	// Validate pin numbers
	if(pinLF < 0 || pinLF >= RPIRover::GPIO_PIN_COUNT) {
		return false;
	}
	if(pinLB < 0 || pinLB >= RPIRover::GPIO_PIN_COUNT) {
		return false;
	}
	if(pinRF < 0 || pinRF >= RPIRover::GPIO_PIN_COUNT) {
		return false;
	}
	if(pinRB < 0 && pinRB >= RPIRover::GPIO_PIN_COUNT) {
		return false;
	}

	// Validate that pins are not already taken by other module
	if(!gpioMap[pinLF]
			&& !gpioMap[pinLB]
			&& !gpioMap[pinRF]
			&& !gpioMap[pinRB]
		)
	{
		gpioMap[pinLF] = true;
		gpioMap[pinLB] = true;
		gpioMap[pinRF] = true;
		gpioMap[pinRB] = true;
	}
	else {
		return false;
	}

	// Register new motor controller
	motorControllers.push_back(
			unique_ptr<ConfigMotorController>(
					new ConfigMotorController(pinLF, pinLB, pinRF, pinRB)
	));

	return true;
}

string ConfigLoader::trim(const string &s) {
	string::const_iterator it = s.begin();
	// Trim begin of string
	while(it != s.end() && isspace(*it)) {
		++it;
	}

	string::const_reverse_iterator rit = s.rbegin();
	// Trim end of string
	while(rit.base() != it && isspace(*rit)) {
		++rit;
	}
	return string(it, rit.base());
}


char *ConfigLoader::getAddress() {
	return addr;
}

uint16_t ConfigLoader::getPort() {
	return port;
}

bool ConfigLoader::pinAvailable(unsigned int index) {
	if(index >= RPIRover::GPIO_PIN_COUNT) {
		return false;
	}
	return !gpioMap[index];
}

list<unique_ptr<ConfigMotorController>> &ConfigLoader::getMotorControllers() {
	return motorControllers;
}

ConfigLoader::~ConfigLoader() {
}

