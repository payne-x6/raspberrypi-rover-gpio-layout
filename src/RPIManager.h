/*
 * RPIManager.h
 *
 *  Created on: Mar 28, 2016
 *  Author: Jan Hak
 *
 * Singleton for access GPIO manager stored in shared memmory
 */

#ifndef RPIMANAGER_H_
#define RPIMANAGER_H_

#include "GPIOManager.h"

#include <iostream>

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/mman.h>

// Path to file that represents space of shared memmory 
#define FILEPATH "/tmp/RPI-Manager"
// Size of shared memmory file
#define FILESIZE (sizeof(GPIOManager))

class RPIManager {
private:
    // File descriptor of shared memmory file
	int fd;
	// Pointer on virtual address of shared memmory
	GPIOManager *gpioManager;

	// Constructor is private to make it Singleton
	RPIManager();
	~RPIManager();
public:
    /**
	 * Return instance of RPIManager singleton
	 * @return Singleton of this class
	 **/
	static RPIManager &getInstance();
	/**
	 * Returns reference on GPIO manager
	 * @return Regerence on class that stores GPIO pins
	 **/
	GPIOManager &getGPIOManager();

	// Delete this method to unable use them - keep it Singleton
	RPIManager(RPIManager const &) = delete;
	void operator=(RPIManager const &) = delete;
};

#endif /* RPIMANAGER_H_ */
