/**
 * Name        : RPIRover.cpp
 * Author      : Jan Hak
 *
 * Program for remote controll of Raspberry Pi over network
**/
#include "ConfigLoader.h"
#include "ConfigMotorController.h"
#include "TCPRemoteClient.h"
#include "RPIConstants.h"

#include <iostream>
#include <memory>

#include <unistd.h>

using namespace std;

TCPRemoteClient *initConnection(ConfigLoader &config);

int main() {
	// Test that user is root or sudoer
	if(geteuid() != 0) {
		cerr << "ERROR: Run application as root" << endl;
		return 1;
	}

	unique_ptr<TCPRemoteClient> network;
	try {
		// Load configuration
		ConfigLoader config;
		// Set new TCPRemoteClient
		network.reset(initConnection(config));
	}
	catch (LoadConfigurationException &e) {
		cerr << "ERROR: " << e.what() << endl;
		return 2;
	}
	catch (ConnectionErrorException &e) {
		cerr << "ERROR: " << e.what() << endl;
		return 3;
	}

	// Start client-server communication
	network->start();
	// Wait till communication is ended
	network->wait();

	return 0;
}


TCPRemoteClient *initConnection(ConfigLoader &config) {
	// Create new TCPRemoteClient
	TCPRemoteClient *client = new TCPRemoteClient(config.getAddress(), config.getPort());
	// Add access permision rest of GPIO pins that are not used by modules
	for(int i = 0; i < RPIRover::GPIO_PIN_COUNT; i++) {
		if(config.pinAvailable(i)) {
			client->addGPIO(i);
		}
	}
	
	int id = 1;
	// Add motor controllers
	for(auto it = config.getMotorControllers().begin(); it != config.getMotorControllers().end(); ++it) {
		client->addMotorController(
				id,
				(*it)->leftForwardPin(),
				(*it)->leftBackwardPin(),
				(*it)->rightForwardPin(),
				(*it)->rightBackwardPin()
		);
		id++;
	}
	return client;
}
