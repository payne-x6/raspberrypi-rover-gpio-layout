/*
 * GPIOManager.cpp
 *
 *  Created on: 16. 9. 2015
 *  Author: Jan Hak
 */

#include "GPIOManager.h"


GPIOManager::GPIOManager() {
	wiringPiSetup();
}

GPIOPin *GPIOManager::getPin(int id) {
	if(id >= 0 && id < RPIRover::GPIO_PIN_COUNT) {
		return &(pins[id]);
	}
	return NULL;
}
