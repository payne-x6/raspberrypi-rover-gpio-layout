/*
 * ConfigMotorController.cpp
 *
 *  Created on: Mar 27, 2016
 *  Author: Jan Hak
 */

#include "ConfigMotorController.h"

ConfigMotorController::ConfigMotorController() {
	this->lf = -1;
	this->lb = -1;
	this->rf = -1;
	this->rb = -1;
}

ConfigMotorController::ConfigMotorController(uint8_t lf, uint8_t lb, uint8_t rf, uint8_t rb) {
	this->lf = lf;
	this->lb = lb;
	this->rf = rf;
	this->rb = rb;
	if(!validArgs()) {
		throw WrongArgumentsException();
	}
}

bool ConfigMotorController::validArgs() {
	//Validate PIN Value
	if(lf >= RPIRover::GPIO_PIN_COUNT
			|| lb >= RPIRover::GPIO_PIN_COUNT
			|| rf >= RPIRover::GPIO_PIN_COUNT
			|| rb >= RPIRover::GPIO_PIN_COUNT
		)
	{
		return false;
	}

	//Validate if PINS are same
	if(lf == lb || lf == rf || lf == rb) {
		return false;
	}
	if(lb == rf || lb == rb) {
		return false;
	}
	if(rf == rb) {
		return false;
	}

	return true;
}

uint8_t ConfigMotorController::leftForwardPin() {
	return lf;
}

uint8_t ConfigMotorController::leftBackwardPin() {
	return lb;
}

uint8_t ConfigMotorController::rightForwardPin() {
	return rf;
}

uint8_t ConfigMotorController::rightBackwardPin() {
	return rb;
}

ConfigMotorController::~ConfigMotorController() {
}

