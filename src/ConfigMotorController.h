/*
 * ConfigMotorController.h
 *
 *  Created on: Mar 27, 2016
 *  Author: Jan Hak
 */

#ifndef CONFIGMOTORCONTROLLER_H_
#define CONFIGMOTORCONTROLLER_H_

#include "RPIConstants.h"
#include "WrongArgumentsException.h"

#include <cstdint>

class ConfigMotorController {
private:
	// Pin of left motor for move forward
	uint8_t lf;
	// Pin of left motor for move backward
	uint8_t lb;
	// Pin of right motor for move forward
	uint8_t rf;
	// Pin of right motor for move backward
	uint8_t rb;

	/**
	 * Test pins of motors for collision
	 * @return True when valid
	 **/
	bool validArgs();
public:
	ConfigMotorController();
	/**
	 * Constructor that set up pins and validate its pins
	 * @param Left forward pin
	 * @param Left Backward pin
	 * @param Right Forward pin
	 * @param Right Backward pin
	 * @throw WrongArgumentsException when parameter are in collision
	 **/
	ConfigMotorController(uint8_t, uint8_t, uint8_t, uint8_t);
	~ConfigMotorController();
	
	/**
	 * Return pin of left motor for move forward
	 * @return Id of left forward pin
	 */
	uint8_t leftForwardPin();
	/**
	 * Return pin of left motor for move backward
	 * @return Id of left backward pin
	 */
	uint8_t leftBackwardPin();
	/**
	 * Return pin of right motor for move forward
	 * @return Id of right forward pin
	 */
	uint8_t rightForwardPin();
	/**
	 * Return pin of right motor for move backward
	 * @return Id of right backward pin
	 */
	uint8_t rightBackwardPin();
};

#endif /* CONFIGMOTORCONTROLLER_H_ */
