/*
 * MotorController.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *  
 * Store pointer of 4 pins that are used for controlling direction and speed of rover motors. Convert angle and speed of movement on PWM values 
 */

#ifndef SRC_MOTORCONTROLLER_H_
#define SRC_MOTORCONTROLLER_H_

#include "GPIOManager.h"
#include "GPIOPin.h"
#include "RPIManager.h"
#include "WrongArgumentsException.h"

#include <cstdint>
#include <cmath>

class MotorController {
private:
	// Constant for converting Angle to radians
	static const double toRad;
	// Pointer on pins connected to motor controller
	GPIOPin *pin[4];
public:
    /**
	 * Create instance of new motor controller
	 *
	 * @param ID of pin for movement of left motor forward
	 * @param ID of pin for movement of left motor backward
	 * @param ID of pin for movement of right motor forward
	 * @param ID of pin for movement of right motor backward
	 * @throw WrongArgumentException when any of pins does not exist
	 **/
	MotorController(uint8_t, uint8_t, uint8_t, uint8_t);
	~MotorController();
	/**
	 * Converts angle and speed of movement on 4 PWM values and setup that PWM signal on GPIO pins
	 * @param Angle of movement
	 * @param Speed of movement
	 **/
	void setDirection(int, int);
};

#endif /* SRC_MOTORCONTROLLER_H_ */
