/*
 * MotorController.cpp
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 */

#include "MotorController.h"

const double MotorController::toRad = M_PI / 180.;


MotorController::MotorController(uint8_t pinLF, uint8_t pinLB, uint8_t pinRF, uint8_t pinRB) {
	GPIOManager &manager = RPIManager::getInstance().getGPIOManager();

	pin[0] = manager.getPin(pinLF);
	pin[1] = manager.getPin(pinLB);
	pin[2] = manager.getPin(pinRF);
	pin[3] = manager.getPin(pinRB);

	//validate
	for(int i = 0; i < 4; i++) {
		if(!pin[i]) {
			throw WrongArgumentsException();
		}
	}

	//set pins PWM
	try {
		for(int i = 0; i < 4; i++) {
			pin[i]->setType(RPIRover::RPI_OUTPUT_PWM);
		}
	}
	catch (IllegalGPIOStateException &ex) {
		throw WrongArgumentsException();
	}
}

MotorController::~MotorController() {

}


void MotorController::setDirection(int speed, int angle) {
	int speeds[4];
	for(int i = 0; i < 4; i++) speeds[i] = 0;

	// Count speed on left motor and cut it on interval <-1, 1>
	double speed1 = M_SQRT2 * sin(((angle + 45) % 360) * toRad);
	speed1 = max(-1., min(1., speed1));

	// Count speed on right motor and cut it on interval <-1, 1>
	double speed2 = M_SQRT2 * sin(((angle + 135) % 360) * toRad);
	speed2 = max(-1., min(1., speed2));

	//LEFT MOTOR FRONT - BACKWARD
	if(speed1 > 0) {
		speeds[0] = speed * speed1;
		speeds[1] = 0;
	}
	else {
		speeds[0] = 0;
		speeds[1] = -speed * speed1;
	}

	//RIGHT MOTOR FRONT - BACKWARD
	if(speed2 > 0) {
		speeds[2] = speed * speed2;
		speeds[3] = 0;
	}
	else {
		speeds[2] = 0;
		speeds[3] = -speed * speed2;
	}

	// Set speeds
	for(int i = 0; i < 4; i++) {
		pin[i]->setValue(speeds[i]);
	}
}

