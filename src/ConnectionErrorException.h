/*
 * ConnectionErrorException.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *
 *	Exception thrown when connection was unable to establish
 */

#ifndef CONNECTION_ERROR_EXCEPTION_H_
#define CONNECTION_ERROR_EXCEPTION_H_

#include <exception>
#include <string>

using namespace std;

class ConnectionErrorException : public std::exception {
private:
	// Text of exception
	string text;
public:
	ConnectionErrorException()
		: text("Unable to create network connection") {};
	ConnectionErrorException(const char *ex) : text(ex){};
	virtual const char* what() const throw() {
		return text.c_str();
	}
};

#endif /** CONNECTION_ERROR_EXCEPTION_H_ **/
