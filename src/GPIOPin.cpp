/*
 * GPIOPin.cpp
 *
 *  Created on: Oct 13, 2015
 *  Author: Jan Hak
 */

#include "GPIOPin.h"

int GPIOPin::COUNT = 0;

GPIOPin::GPIOPin() {
	sem_init(&lock, 1, 1);

	// Set GPIO pin ID
	pin = GPIOPin::COUNT;
	GPIOPin::COUNT++;

	// Set default values
	setType(RPIRover::RPI_OUTPUT);
	setValue(0);
}

GPIOPin::~GPIOPin() {
}


int GPIOPin::getPin() {
	return pin;
}

RPIRover::GPIOType GPIOPin::getType() {
	return type;
}

void GPIOPin::setType(RPIRover::GPIOType type) {
	if(this->type == type) {
		return;
	}
	
	sem_wait(&lock);
	// Set type output
	if (type == RPIRover::RPI_OUTPUT) {
		if(this->type == RPIRover::RPI_OUTPUT_PWM) {
			sem_post(&lock);
			throw IllegalGPIOStateException();
		}

		pinMode(pin, OUTPUT);
		this->type = type;
	}
	// Set type input
	else if (type == RPIRover::RPI_INPUT) {
		if(this->type == RPIRover::RPI_OUTPUT_PWM) {
			sem_post(&lock);
			throw IllegalGPIOStateException();
		}
		pinMode(pin, INPUT);
		this->type = type;
	}
	// Set type pwm output
	else if (type == RPIRover::RPI_OUTPUT_PWM) {
		if(softPwmCreate(pin, 0, 100)) {
			sem_post(&lock);
			throw IllegalGPIOStateException();
		}
		this->type = type;
	}
	sem_post(&lock);
}

int GPIOPin::getValue() {
	// If input then read value
	if(type == RPIRover::RPI_INPUT) {
		return read();
	}
	// Else return stored (setup) value
	else {
		return value;
	}
}

void GPIOPin::setValue(int value) {
	sem_wait(&lock);
	switch (type) {
		case RPIRover::RPI_OUTPUT:
			this->value = value ? 1 : 0;
			digitalWrite(pin, this->value);
			break;
		case RPIRover::RPI_OUTPUT_PWM:
			this->value = max(0, min(100, value));
			softPwmWrite(pin, this->value);
			break;
		default:
			sem_post(&lock);
			throw IllegalGPIOStateException();
			break;
	}
	sem_post(&lock);
}


int GPIOPin::read() {
	if(type == RPIRover::RPI_INPUT) {
		int read = digitalRead(pin);
		return read;
	} else {
		throw IllegalGPIOStateException();
	}
}

