/*
 * RPIConstants.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *  
 * Header that store constant variables and enumerations, that are used in various places in project. Make easier to access them and modify them
 */
#ifndef RPI_CONSTANTS_H_
#define RPI_CONSTANTS_H_

namespace RPIRover {
    // Number of GPIO Pins
	const char GPIO_PIN_COUNT = 26;
	// Enumeration that represents available setup of GPIO types
	enum GPIOType {
		RPI_OUTPUT = 0,
		RPI_INPUT = 1,
		RPI_OUTPUT_PWM = 2
	};
}

#endif
