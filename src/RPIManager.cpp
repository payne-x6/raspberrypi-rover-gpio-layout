/*
 * RPIManager.cpp
 *
 *  Created on: Mar 28, 2016
 *  Author: Jan Hak
 */

#include "RPIManager.h"

RPIManager::RPIManager() {
	int result;
	bool isNew = true;

	// Try to create file for shared memory
	fd = open(FILEPATH, O_RDWR | O_CREAT | O_EXCL, (mode_t)0600);
	if(fd == -1) {
		// File already exist
		if(errno == EEXIST) {
			isNew = false;
		}
		else {
			std::cerr << "ERROR: Unable to load RPIManager mmaped file" << std::endl;
			exit(128);
		}
	}

	// Open file
	if(!isNew) {
		fd = open(FILEPATH, O_RDWR, (mode_t)0600);
		if(fd == -1) {
			std::cerr << "ERROR: Unable to load RPIManager mmaped file" << std::endl;
			exit(128);
		}
	}
	
	// Set size of created file
	if(isNew) {
		result = lseek(fd, FILESIZE - 1, SEEK_SET);
		if(result == -1) {
			close(fd);
			std::cerr << "ERROR: Unable to load RPIManager mmaped file 3" << std::endl;
			exit(128);
		}

		result = write(fd, "", 1);
		if(result != 1) {
			close(fd);
			std::cerr << "ERROR: Unable to load RPIManager mmaped file 2" << std::endl;
			exit(128);
		}
	}
	
	// Load whole shared memory
	void *mmapPtr = mmap(0, FILESIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if(mmapPtr == MAP_FAILED) {
		close(fd);
		std::cerr << "ERROR: Unable to load RPIManager mmaped file 1" << std::endl;
		exit(128);
	}
	
	// Create new instance of GPIOManager in memory
	if(isNew) {	
		gpioManager = new (mmapPtr) GPIOManager();
	}
	// Load instance of GPIOManager from memory
	else {
		gpioManager = reinterpret_cast<GPIOManager *>(mmapPtr);
	}
}

RPIManager::~RPIManager() {
	munmap(gpioManager, FILESIZE);
	close(fd);
}

RPIManager &RPIManager::getInstance() {
	// Create static instance when not initialized
	static RPIManager instance;
	return instance;
}

GPIOManager &RPIManager::getGPIOManager() {
	return *gpioManager;
}


