/*
 * MessageUnresolvedException.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *
 *	Exception thrown when unknown message was recieved from network socket 
 */

#ifndef MESSAGE_UNRESOLVED_EXCEPTION_H_
#define MESSAGE_UNRESOLVED_EXCEPTION_H_

#include <exception>
#include <string>

using namespace std;

class MessageUnresolvedException : public exception {
private:
    // Text of exception
	string text;
public:
	MessageUnresolvedException()
		: text("Unable to resolve message") {};
	MessageUnresolvedException(const char *ex) : text(ex){};
	virtual const char* what() const throw() {
		return text.c_str();
	}
};

#endif /** MESSAGE_UNRESOLVED_EXCEPTION_H_ **/
