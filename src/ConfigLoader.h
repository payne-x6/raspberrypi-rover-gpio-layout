/*
 * ConfigLoader.h
 *
 *  Created on: Mar 14, 2016
 *  Author: Jan Hak
 *
 *	Load configuration from file, parse and store its data 
 */

#ifndef SRC_CONFIGLOADER_H_
#define SRC_CONFIGLOADER_H_

#include "RPIConstants.h"
#include "LoadConfigurationException.h"
#include "ConfigMotorController.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include <list>
#include <memory>
#include <climits>
#include <cinttypes>

#include <netdb.h>
#include <arpa/inet.h>

using namespace std;

class ConfigLoader {
private:
	// Delimiter of data
	static const string delimiter;
	// Array of flags, that signals if pin was already used in some module
	bool gpioMap[RPIRover::GPIO_PIN_COUNT];
	// Flag signals port and IP were set
	bool ipSet, portSet;

	// IP address of server
	char addr[INET6_ADDRSTRLEN];
	// Port of server
	uint16_t port;
	// List of available motor controllers
	list<unique_ptr<ConfigMotorController>> motorControllers;

	/**
	 * Method that parse each line of configuration file
	 * @param Number of line
	 * @param Text of line
	 **/
	void parseLine(const unsigned int, const string &);
	/**
	 * Method that trim string (remove white characters from begin and end of line)
	 * @param Input string
	 * @return Trimed string
	 **/
	string trim(const string &);

	/**
	 * Method that validate and save IP address of server
	 * @param IP addr of domain name of server as string
	 * @return Set successful
	 **/
	bool ipAddrBind(const string &);
	/**
	 * Method that validate and save port of server
	 * @param port of server as string
	 * @return Set successful
	 **/
	bool ipPortBind(const string &);
	/**
	 * Method that validate and save settings of motor controller
	 * @param Pins of motor controller separated by delimiter passed as string
	 * @return Set successful
	 **/
	bool addMotorController(const string &);
public:
	/**
	 * Create new instance of ConfilLoader, read config file and store its data
	 * @throw LoadConfigurationException when can't parse important data from file or find error in file
	 **/
	ConfigLoader();
	~ConfigLoader();

	/**
	 * Return IP address of server as pointer on char array of maximal size of IPv6
	 * @return Pointer on array with IP address of server
	 **/
	char *getAddress();
	/**
	 * Return port of server as 2B number
	 * @return Port of server
	 */
	uint16_t getPort();
	/**
	 * Return true, if port wasn't used by any module
	 * @param ID of GPIO pin
	 * @return Is taken by module
	 **/
	bool pinAvailable(unsigned int);
	/**
	 * Return reference on list of motor controllers parsed from configuration file
	 * @return Reference on list of motor controllers
	 **/
	list<unique_ptr<ConfigMotorController>> &getMotorControllers();
};

#endif /* SRC_CONFIGLOADER_H_ */
