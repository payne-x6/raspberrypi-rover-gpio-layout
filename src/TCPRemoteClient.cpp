/*
 * TCPRemoteClient.cpp
 *
 *  Created on: Nov 25, 2015
 * Author: Jan Hak
 */

#include "TCPRemoteClient.h"


TCPRemoteClient::TCPRemoteClient(char* addr, uint16_t port) {
	running = false;

	// Create socket
	client_socket = socket(AF_INET, SOCK_STREAM, 0);
	if(socket < 0) {
		throw ConnectionErrorException();
	}

	// Set myaddr
	memset(&my_addr, 0, sizeof(struct sockaddr_in));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(port);
	my_addr.sin_addr.s_addr = inet_addr(addr);

	// Connect to server
	int connectError = connect(client_socket, (struct sockaddr *)&my_addr, sizeof(struct sockaddr_in));
	if(connectError) {
		throw ConnectionErrorException("Unable to establish the connection");
	}

	struct timeval tv;
	tv.tv_sec = 2;
	tv.tv_usec = 0;
	
	// Set REUSEADDR
	int reuse = 1;
	if(setsockopt(client_socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0) {
		throw ConnectionErrorException("Unable to establish the connection");
	}
	// Set recv timeout
	if(setsockopt(client_socket, SOL_SOCKET, SO_RCVTIMEO, (void *)&tv, sizeof(struct timeval)) < 0) {
		throw ConnectionErrorException("Unable to establish the connection");
	}
}


TCPRemoteClient::~TCPRemoteClient() {
	close(client_socket);
}

void TCPRemoteClient::run() {
	// Reading loop
	while (running) {
		uint8_t cmd = 0;
		int received = recv(client_socket, &cmd, sizeof(uint8_t), 0);
		if(received > 0) {
			try {
				// Resolve message type
				switch(cmd) {
				case SET_GPIO_TYPE:
					recvSetGPIOType();
					break;
				case SET_GPIO_VALUE:
					recvSetGPIOValue();
					break;
				case MODULE_MOTOR_CONTROLLER_DIRECTION_VECTOR:
					recvMotorDirectionVector();
					break;
				case TOOL_GET_SETTINGS:
					recvGetSettings();
					break;
				case TOOL_SHUTDOWN:
					recvShutdown();
					break;
				case TOOL_PING:
					recvPing();
					break;
				case TOOL_PONG:
					recvPong();
					break;
				default:
					throw MessageUnresolvedException();
				}
			}
			catch (MessageUnresolvedException &ex) {

			}
		}
		else if(received == 0) {
			running = false;
		}
		else {
			if(errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			running = false;
		}

	}


}

void TCPRemoteClient::clientNotification() {
	// Notify each 2 secs
	while(running) {
		for(auto it = gpioPins.begin(); it != gpioPins.end(); ++it) {
			char buffer[] = {
					GET_GPIO_TYPE, (char)it->second->getPin(), (char)it->second->getType(), 0,
					GET_GPIO_VALUE, (char)it->second->getPin(), (char)it->second->getValue(), 0
			};
			send(client_socket, buffer, 8, 0);

		}
		sleep(2);
	}
}

void TCPRemoteClient::recvSetGPIOType() {
	uint8_t message[3];
	int returned = recv(client_socket, message, 3*sizeof(uint8_t), 0);
	if(returned <= 2 && message[2] != END_BYTE) {
		throw MessageUnresolvedException();
	}
	GPIOPin *pin = gpioPins[message[0]];
	if(!pin) {
		throw MessageUnresolvedException();
	}
	
	// Set type
	switch (message[1]) {
	case RPIRover::RPI_OUTPUT:
		pin->setType(RPIRover::RPI_OUTPUT);
		break;
	case RPIRover::RPI_INPUT:
		pin->setType(RPIRover::RPI_INPUT);
		break;
	case RPIRover::RPI_OUTPUT_PWM:
		pin->setType(RPIRover::RPI_OUTPUT_PWM);
		break;
	default:
		throw MessageUnresolvedException();
		break;
	}
}

void TCPRemoteClient::recvSetGPIOValue() {
	uint8_t message[3];
	int returned = recv(client_socket, message, 3*sizeof(uint8_t), 0);
	if(returned <= 2 || message[2] != END_BYTE) {
		throw MessageUnresolvedException();
	}
	GPIOPin *pin = gpioPins[message[0]];
	if(!pin) {
		throw MessageUnresolvedException();
	}
	
	// Set value
	pin->setValue(message[1]);
}

void TCPRemoteClient::recvMotorDirectionVector() {
	uint8_t message[5];
	uint16_t angle;
	int returned = recv(client_socket, message, 5*sizeof(uint8_t), 0);
	if(returned <= 4 || message[4] != END_BYTE) {
		throw MessageUnresolvedException();
	}
	
	MotorController *controller = motorControllers[message[0]].get();
	if(!controller) {
		throw MessageUnresolvedException();
	}
	
	// Parse 2B number
	angle = message[1];
	angle <<= 8;
	angle += message[2];

	// Set movement direction
	controller->setDirection(message[3], angle);
}

void TCPRemoteClient::recvGetSettings() {
	uint8_t endByte;
	int returned = recv(client_socket, &endByte, sizeof(uint8_t), 0);
	if(returned <= 0 || endByte != END_BYTE) {
		throw MessageUnresolvedException();
	}

	sleep(1);
	// Send new GPIO pins
	for(auto it = gpioPins.begin(); it != gpioPins.end(); ++it) {
		sendNewGPIO(it->first);
	}
	
	// Send new motor controllers
	for(auto it = motorControllers.begin(); it != motorControllers.end(); ++it) {
		sendNewMotorController(it->first);
	}

}

void TCPRemoteClient::recvShutdown() {
	uint8_t endByte;
	int returned = recv(client_socket, &endByte, sizeof(uint8_t), 0);
	if(returned <= 0 || endByte != END_BYTE) {
		throw MessageUnresolvedException();
	}

	// Stops client listening and norifications
	running = false;
	// Fork process
	if(!fork()) {
		// Close client in parent process
		close(client_socket);
		
		// Wait 5 secs (for end of child process) and then change parent process on shutdown
		const char *const argv[] = {"/usr/bin/sudo", "shutdown", "-h", "now", NULL};
		sleep(5);	
		execvp("/usr/bin/sudo", (char**)argv);
	}
}

void TCPRemoteClient::recvPing() {
	uint8_t endByte;
	int returned = recv(client_socket, &endByte, sizeof(uint8_t), 0);
	if(returned <= 0 || endByte != END_BYTE) {
		throw MessageUnresolvedException();
	}
	
	sendPong();
}

void TCPRemoteClient::recvPong() {
	uint8_t endByte;
	int returned = recv(client_socket, &endByte, sizeof(uint8_t), 0);
	if(returned <= 0 || endByte != END_BYTE) {
		throw MessageUnresolvedException();
	}
}

void TCPRemoteClient::start() {
	if(!running) {
		running = true;
		reciever.reset(new thread(&TCPRemoteClient::run, this));
		notifier.reset(new thread(&TCPRemoteClient::clientNotification, this));
	}
}

void TCPRemoteClient::wait() {
	if(reciever.get()) {
		reciever->join();
		reciever.reset();
	}
	if(notifier.get()) {
		notifier->join();
		notifier.reset();
	}
}

void TCPRemoteClient::stop() {
	if(running) {
		running = false;
	}
	wait();
}

void TCPRemoteClient::addGPIO(unsigned int index) {
	if(index < RPIRover::GPIO_PIN_COUNT) {
		if(!gpioPins[index]) {
			gpioPins[index] = RPIManager::getInstance().getGPIOManager().getPin(index);
		}
	}
}

void TCPRemoteClient::addMotorController(unsigned int id, uint8_t lf, uint8_t lb, uint8_t rf, uint8_t rb) {
	if(!(motorControllers[id].get())) {
		try {
			MotorController *controller = new MotorController(lf, lb, rf, rb);
			motorControllers[id].reset(controller);
		}
		catch (WrongArgumentsException &ex) {

		}
	}

}

void TCPRemoteClient::sendNewGPIO(uint8_t id) {
	char buffer[] = {GET_NEW_GPIO, (char)id, 0};
	send(client_socket, buffer, 3, 0);
}


void TCPRemoteClient::sendNewMotorController(uint8_t id) {
	char buffer[] = {MODULE_MOTOR_CONTROLLER_NEW, (char)id, 0};
	send(client_socket, buffer, 3, 0);
}

void TCPRemoteClient::sendPing() {
	char buffer[] = {TOOL_PING, 0};
	send(client_socket, buffer, 2, 0);

}
void TCPRemoteClient::sendPong() {
	char buffer[] = {TOOL_PONG, 0};
	send(client_socket, buffer, 2, 0);
}
