/*
 * GPIOManager.h
 *
 *  Created on: 16. 9. 2015
 *  Author: Jan Hak
 *
 *  Container of GPIOPins for store them in shared memmory
 */

#ifndef GPIOMANAGER_H_
#define GPIOMANAGER_H_


#include "GPIOPin.h"
#include "RPIConstants.h"

#include <wiringPi.h>

using namespace std;

class GPIOManager {
private:
    // Array of GPIO pins classes
	GPIOPin pins[RPIRover::GPIO_PIN_COUNT];
public:
	GPIOManager();
    /**
     * Return pointer on instance of pin by its index (id). Return NULL when index is out of range
     * @param ID of GPIO pin
     * @return Pointer on instance of GPIOPin class
     **/
	GPIOPin *getPin(int);
};

#endif /* GPIOMANAGER_H_ */
